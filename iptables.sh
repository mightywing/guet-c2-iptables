#!/bin/sh

iptables -F
iptables -X
iptables -Z

iptables -A INPUT -i lo -j ACCEPT 		#环路放行
iptables -A INPUT -i em2 -j ACCEPT	#内部网卡所有数据放行
iptables -A INPUT -i ppp+  -j ACCEPT	#pptp所有数据放行
iptables -A INPUT -p icmp  -j ACCEPT
iptables -A INPUT -p gre -j ACCEPT  #pptp/gre
iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT 


#iptables -A INPUT  -s 10.0.0.1/16   -p tcp -m state --state NEW -m tcp   --dport 22 -j ACCEPT  #ssh
iptables -A INPUT -p tcp -m state --state NEW -m tcp   --dport 1020 -j ACCEPT  #ssh
iptables -A INPUT -p tcp -m state --state NEW -m tcp   --dport 80 -j ACCEPT  #web
iptables -A INPUT -p tcp -m state --state NEW -m tcp   --dport 81 -j ACCEPT  #web
iptables -A INPUT -p tcp -m state --state NEW -m tcp   --dport 88 -j ACCEPT  #web proxy
#iptables -A INPUT -p tcp -m state --state NEW -m tcp   --dport 8000 -j ACCEPT  #svn
iptables -A INPUT -p tcp -m state --state NEW -m tcp   --dport 20:21 -j ACCEPT  #ftp
iptables -A INPUT -p tcp -m state --state NEW -m tcp   --dport 40000:40100 -j ACCEPT  #ftp

#iptables -A INPUT -p tcp -m state --state NEW -m tcp   --dport 8080 -j ACCEPT  #tomcat
#iptables -A INPUT -p tcp -m state --state NEW -m tcp   --dport 8983 -j ACCEPT   #solr
iptables -A INPUT -p tcp -m state --state NEW -m tcp   --dport 1180 -j ACCEPT  
iptables -A INPUT -p tcp -m state --state NEW -m tcp   --dport 1181 -j ACCEPT  
iptables -A INPUT -p tcp -m state --state NEW -m tcp   --dport 5000 -j ACCEPT #  1408 storage

#proxy
#iptables -A INPUT   -s 202.193.73.0/24  -d 202.103.243.122 -p tcp -m state --state NEW -m tcp   --dport 1080 -j ACCEPT
#iptables -A INPUT   -s 172.16.13.0/24  -d 202.103.243.122 -p tcp -m state --state NEW -m tcp   --dport 1080 -j ACCEPT

#mysql
#iptables -A INPUT -d 10.0.0.20 -p tcp -m state --state NEW -m tcp   --dport 3306 -j ACCEPT

#webmin
#iptables -A INPUT -p TCP --dport 10000 -j ACCEPT

#docker 
iptables -A INPUT -i docker0   -j ACCEPT

#openvpn
iptables -A INPUT -p tcp -m state --state NEW -m tcp   --dport 1194 -j ACCEPT  
iptables -A INPUT -p udp -m state --state NEW -m udp   --dport 1194 -j ACCEPT  
iptables -A INPUT -p TCP --dport 1194 -j ACCEPT
iptables -A INPUT -p TCP --dport 7505 -j ACCEPT

#gogs
#iptables -A INPUT -p TCP --dport 3000 -j ACCEPT

#l2tp
iptables -A INPUT -p 50 -j ACCEPT
iptables -A INPUT -p udp  --dport 500 -j ACCEPT
iptables -A INPUT -p udp  --dport 4500 -j ACCEPT
iptables -A INPUT -p udp  --dport 1701 -j ACCEPT
iptables -A INPUT -p udp  --dport 51 -j ACCEPT

iptables -A INPUT -p tcp -m state --state NEW -m tcp   --dport 1723 -j ACCEPT  #pptpd
iptables -A INPUT -p tcp  --dport 47 -j ACCEPT  #gre
iptables -A INPUT -p udp  --dport 47 -j ACCEPT  #gre

#ffserver
iptables -A INPUT -p tcp  --dport 8090 -j ACCEPT

#docker red5
iptables -A INPUT -p tcp  --dport 843 -j ACCEPT
iptables -A INPUT -p tcp  --dport 5080 -j ACCEPT
iptables -A INPUT -p tcp  --dport 5443 -j ACCEPT
iptables -A INPUT -p tcp  --dport 1935 -j ACCEPT
iptables -A INPUT -p tcp  --dport 8443 -j ACCEPT
iptables -A INPUT -p tcp  --dport 8088 -j ACCEPT
iptables -A INPUT -p tcp  --dport 9999 -j ACCEPT
iptables -A INPUT -p tcp  --dport 6666 -j ACCEPT
iptables -A INPUT -p tcp  --dport 554 -j ACCEPT

#监控宝 snmp
#iptables -A INPUT -i eth0 -p udp -s 60.195.252.107 --dport 161 -j ACCEPT
#iptables -A INPUT -i eth0 -p udp -s 60.195.252.110 --dport 161 -j ACCEPT

#ntpd
#iptables -A INPUT  -p udp   -m state --state NEW -m udp   --dport 123 -j ACCEPT

#ftp
#iptables -A INPUT -p tcp -m state --state NEW -m tcp   --dport 20:21 -j ACCEPT
#iptables -A INPUT -p tcp -m state --state NEW -m tcp   --dport 50000:51000 -j ACCEPT

#openvpn
iptables --table nat -A POSTROUTING -s 10.8.0.0/24 -o em1 -j SNAT --to-source 202.103.243.122

#iptables --table nat -A POSTROUTING -s 10.0.0.0/24 -o eth0 -j MASQUERADE
iptables --table nat -A POSTROUTING -s 10.0.0.0/24 -o em1 -j SNAT --to-source 202.103.243.122
iptables --table nat -A POSTROUTING -s 10.0.1.0/24 -o em1 -j SNAT --to-source 202.103.243.122
iptables --table nat -A POSTROUTING -s 10.0.2.0/24 -o em1 -j SNAT --to-source 202.103.243.122
iptables --table nat -A POSTROUTING -s 10.10.10.0/24 -o em1 -j SNAT --to-source 202.103.243.122

iptables -t nat -A POSTROUTING -o em2  -j SNAT --to 10.0.0.20
iptables -t nat -A POSTROUTING -o em1  -j SNAT --to 202.103.243.122

#iptables -A FORWARD -p tcp --syn -s 10.0.0.0/16 -j TCPMSS --set-mss 1356
#iptables -A FORWARD -p tcp --syn -s 10.0.1.0/16 -j TCPMSS --set-mss 1356
#iptables -A FORWARD -p tcp --syn -s 10.0.2.0/16 -j TCPMSS --set-mss 1356
#iptables -A FORWARD -p tcp --syn -s 10.8.0.0/24 -j TCPMSS --set-mss 1356
#iptables -A FORWARD -p tcp --syn -s 10.10.10.0/24 -j TCPMSS --set-mss 1356
#iptables -I FORWARD -p tcp --syn -i ppp+ -j TCPMSS --set-mss 1356


#fms
#iptables -A INPUT -p tcp -m state --state NEW -m tcp   --dport 1935 -j ACCEPT  
#iptables -A INPUT -p tcp -m state --state NEW -m tcp   --dport 1111 -j ACCEPT  

#iptables -t nat -A PREROUTING   -d 202.103.243.122 -p tcp --dport 1935  -j DNAT --to-destination 10.0.0.2:1935
#iptables -t nat -A PREROUTING   -d 202.103.243.122 -p tcp --dport 1111  -j DNAT --to-destination 10.0.0.2:1111

#dnat webdeploy 五网合一发布 
iptables -t nat -A PREROUTING   -d 202.103.243.122 -p tcp --dport 7230  -j DNAT --to-destination 10.0.0.30:8172
iptables -A INPUT -p tcp -m state --state NEW -m tcp   --dport 7230 -j ACCEPT

#svn
#iptables -t nat -A PREROUTING   -d 202.103.243.122 -p tcp --dport 8000  -j DNAT --to-destination 120.24.223.54:8000


#彭玉元网站远程桌面
#iptables -A INPUT -p tcp -m state --state NEW -m tcp   --dport 10151 -j ACCEPT
#iptables -t nat -A PREROUTING -d 202.103.243.122 -p tcp --dport 10151  -j DNAT --to-destination 172.16.64.57:3389
#大学生在线
iptables -A INPUT -p tcp -m state --state NEW -m tcp   --dport 11138 -j ACCEPT
iptables -t nat -A PREROUTING -d 202.103.243.122 -p tcp --dport 11138 -j DNAT --to-destination 10.0.0.38:3389


#广西师范大学辅导员服务器
#iptables -A INPUT -p tcp -m state --state NEW -m tcp   --dport 11389 -j ACCEPT
#iptables -t nat -A PREROUTING -d 202.103.243.122 -p tcp --dport 11389 -j DNAT --to-destination 172.16.64.59:3389

#奥派电子政务
iptables -A INPUT -p tcp -m state --state NEW -m tcp   --dport 12330 -j ACCEPT
iptables -t nat -A PREROUTING -d 202.103.243.122 -p tcp --dport 12330 -j DNAT --to-destination 10.0.0.45:3389



#mighty的远程桌面
iptables -A INPUT -p tcp -m state --state NEW -m tcp   --dport 5001  -j ACCEPT
iptables -t nat -A PREROUTING -d 202.103.243.122 -p tcp --dport 5001 -j DNAT --to-destination 10.0.0.52:5000
iptables -A INPUT -p tcp -m state --state NEW -m tcp   --dport 443  -j ACCEPT
iptables -t nat -A PREROUTING -d 202.103.243.122 -p tcp --dport 443 -j DNAT --to-destination 10.0.0.52:443
iptables -A INPUT -p tcp -m state --state NEW -m tcp   --dport 1122  -j ACCEPT
iptables -t nat -A PREROUTING -d 202.103.243.122 -p tcp --dport 1122 -j DNAT --to-destination 10.0.0.52:1122
iptables -A INPUT   -p tcp -m state --state NEW -m tcp   --dport 5055  -j ACCEPT
iptables -t nat -A PREROUTING -d 202.103.243.122 -p tcp --dport 5055 -j DNAT --to-destination 10.0.0.8:5055
iptables -t nat -A OUTPUT -d 202.103.243.122 -p tcp --dport 5055 -j DNAT --to-destination 10.0.0.8:5055
iptables -A INPUT -p tcp -m state --state NEW -m tcp   --dport 8530  -j ACCEPT
iptables -t nat -A PREROUTING -d 202.103.243.122 -p tcp --dport 8530 -j DNAT --to-destination 10.0.0.30:8530
iptables -A INPUT -p tcp -m state --state NEW -m tcp   --dport 6690  -j ACCEPT
iptables -t nat -A PREROUTING -d 202.103.243.122 -p tcp --dport 6690 -j DNAT --to-destination 10.10.10.245:6690
iptables -t nat -A OUTPUT -d 202.103.243.122 -p tcp --dport 6690 -j DNAT --to-destination 10.10.10.245:6690
iptables -A INPUT -p tcp -m state --state NEW -m tcp   --dport 3306  -j ACCEPT
iptables -t nat -A PREROUTING -d 202.103.243.122 -p tcp --dport 3306 -j DNAT --to-destination 10.0.0.29:3306
#iptables -A INPUT   -s 202.193.73.0/24  -d 202.103.243.122 -p tcp -m state --state NEW -m tcp   --dport 1080 -j ACCEPT

#网警支队软件开放端口
#iptables -A INPUT -p tcp -m state --state NEW -m tcp   --dport 9500  -j ACCEPT
#iptables -t nat -A PREROUTING -d 202.103.243.122 -p tcp --dport 9500 -j DNAT --to-destination 10.0.0.90:9500
#iptables -A INPUT -p tcp -m state --state NEW -m tcp   --dport 9300  -j ACCEPT
#iptables -t nat -A PREROUTING -d 202.103.243.122 -p tcp --dport 9300 -j DNAT --to-destination 10.0.0.90:9300
#iptables -A INPUT -p tcp -m state --state NEW -m tcp   --dport 9700  -j ACCEPT
#iptables -t nat -A PREROUTING -d 202.103.243.122 -p tcp --dport 9700 -j DNAT --to-destination 10.0.0.90:9700

iptables -I FORWARD -p tcp --syn -i ppp+ -j TCPMSS --set-mss 1356

#禁止IP
iptables -I INPUT -s 66.76.9.15 -j DROP
iptables -I INPUT -s 10.10.50.46 -j DROP
iptables -I INPUT -s 75.125.28.111 -j DROP
#iptables -I INPUT -s 125.217.40.250 -j DROP
iptables -I INPUT -s 180.97.152.51 -j DROP
iptables -I INPUT -s 221.44.217.125 -j DROP
#尧山IP限速
#iptables -A OUTPUT -d 125.217.44.221/32 -m limit --limit 10000/s -j ACCEPT
#iptables -A OUTPUT -d 125.217.44.221/32 -j DROP
#

#默认丢弃所有包
iptables -P INPUT DROP
#iptables -P INPUT ACCEPT
